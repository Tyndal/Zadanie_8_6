// Variables

var a = Number(prompt('Enter "a" variable'));
var b = Number(prompt('Enter "b" variable'));
var value = (a * a) - (2 * a * b) - (b * b);

// Content

console.log(value);

if (value > 0) {
  alert('Wynik dodatni!');
}
else if (value < 0) {
  alert('Wynik ujemny!');
}
else {
  alert('Jesteśmy na zerze!');
}